﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraControl : MonoBehaviour {

    public float rotateSpeed = 0.025f;
    Vector3 rotateTranslation;
	public Transform target; 
	public Transform player;

    // Update is called once per frame
    void Update()
    {

		transform.LookAt (target);


    }
}
