﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    //Components
    private Animator animator;
    private new BoxCollider collider;

    //Combat Variables
	private float health;
    private bool dead = false;


	void Start()
    {
        animator = GetComponent<Animator>();
        collider = GetComponentInChildren<BoxCollider>();

		health = 15f;
    }

    IEnumerator delayDestroy()
    {
        yield return new WaitForSeconds(5f);

        Destroy(this.gameObject);
    }

	public void loseHealth(float damage)
    {
		health -= damage;

        if (health < 0)
        {
            dead = true;
            collider.enabled = false;
            animator.SetBool("dead", dead);
            StartCoroutine("delayDestroy");
        }
	}

    void Update()
    {

    }

}
