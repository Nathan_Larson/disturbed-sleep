﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour {

    public static bool paused = false;

    public GameObject pauseMenu;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        { 
            paused = !paused;
            pauseMenu.SetActive(paused);
        }

        if (paused)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

    }
}
