﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CircleMove : GoalMove {

    public float radIncrement;
    public float distanceDecrement;
    protected float rad;
    protected float distance;

    protected override void InitGoal()
    {
        target = GameObject.Find("Beast").transform;
        Vector3 other = target.transform.position;
        Vector3 me = transform.position;
        distance = Vector3.Distance(me, other);

        rad = Mathf.Atan2(other.x - me.x, other.z - me.z);
        SetGoal();
    }

    protected override void SetGoal()
    {
        //find a goal to visit enroute to the target
        Vector3 other = target.transform.position;
        distance = Mathf.Max(distance - distanceDecrement, 0);
        ChangeRadians();
        //correct radians
        if (rad > 2 * Mathf.PI) {
            rad -= 2 * Mathf.PI;
        } else if (rad < 0) {
            rad += 2 * Mathf.PI;
        }
        //set goal vector
        float x = other.x - (distance * Mathf.Sin(rad));
        float z = other.z + (distance * Mathf.Cos(rad));
        goal = new Vector3(x, 0, z);
    }

    //Change rad using radIncrement
    protected abstract void ChangeRadians();
}
