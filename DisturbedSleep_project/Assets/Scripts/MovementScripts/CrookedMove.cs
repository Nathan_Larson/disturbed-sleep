﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrookedMove : CircleMove {

    protected override void ChangeRadians()
    {
        //50% change the direction will change
        if (0.5f < Random.Range(0.0f, 1.0f)){
            radIncrement = radIncrement * -1;
        }
        rad = rad + radIncrement;
    }
}
