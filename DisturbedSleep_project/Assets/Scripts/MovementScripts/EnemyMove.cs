﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyMove : MonoBehaviour {

    public Transform target;
    public float stopDistance;

    protected UnityEngine.AI.NavMeshAgent agent;


    // Use this for initialization
    void Start()
    {
        target = GameObject.Find("Beast").transform;
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //move towards target unless close enough to stop
        if (Vector3.Distance(transform.position, target.transform.position) > stopDistance)
        {
            Move();
        }
        else
        {
            agent.SetDestination(transform.position);
        }
    }

    //Makes the NavMesgAgent reach the goal
    public abstract void Move();
}
