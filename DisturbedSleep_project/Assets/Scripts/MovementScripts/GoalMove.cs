﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GoalMove : EnemyMove {
    protected Vector3 goal;

    // Use this for initialization
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        InitGoal();
    }

    //Set up the script
    protected abstract void InitGoal();

    //Change the Goal
    protected abstract void SetGoal();


    public override void Move()
    {
        //move towards the goal
        if (Vector3.Distance(transform.position, goal) > stopDistance)
        {
            agent.SetDestination(goal);
        }
        else
        {
            //change goal
            SetGoal();
        }

    }


}
