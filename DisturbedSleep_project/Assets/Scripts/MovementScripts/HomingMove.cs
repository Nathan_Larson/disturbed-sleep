﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingMove : EnemyMove {

    public override void Move()
    {
        //go straight to the target
        agent.SetDestination(target.position);
    }
}
