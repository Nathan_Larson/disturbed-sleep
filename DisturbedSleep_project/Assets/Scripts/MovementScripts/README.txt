How to use the scripts by Elroy

You need to make:

1)An Unity Navigation Mesh in the game world

2)Enemies with Nav Mesh Agents

How to use:

Give an enemy a script and fill in the variables

EnemyMove, GoalMove, and CircleMove are abstract so don't try adding them.

All scripts require:

1)A stop distance which the distance they stop before the target in Unity units

2)A target

All CircleMove subclasses also need:

1)A rad increment in radians (between zero and 2PI)

2)A distance decrement in unity units