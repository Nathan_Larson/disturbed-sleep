﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralMove : CircleMove
{
    protected override void ChangeRadians()
    {
        //move in a circle
        rad = rad + radIncrement;
    }
}
