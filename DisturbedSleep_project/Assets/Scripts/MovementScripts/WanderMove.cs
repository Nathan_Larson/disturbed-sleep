﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderMove : GoalMove {
    

    protected override void InitGoal()
    {
        //find a goal to visit before heading to target
        Vector3 other = target.transform.position;
        float radius = Vector3.Distance(transform.position, other) / 2;
        float rad = UnityEngine.Random.Range(0.0f, Mathf.PI * 2);
        float x = other.x - (radius * Mathf.Sin(rad));
        float z = other.z + (radius * Mathf.Cos(rad));
        goal = new Vector3(x, 0, z);
    }

    protected override void SetGoal()
    {
        //set goal to target
        goal = target.transform.position;
    }
    
}
