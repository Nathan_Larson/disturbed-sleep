﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigZagMove : CircleMove{

    protected override void ChangeRadians()
    {
        radIncrement = radIncrement * -1;
        rad = rad + radIncrement;
    }
}
