﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour {

    //Components
    private Animator animator;

    //Raycast Variables
    private LayerMask enemyMask;
    private float yOffset = 0.39f;

    //Light Variables
    public Light combatLight;
    public ParticleSystem particles;
    public ParticleSystem particles2;
    public float baseIntensity = 1;

    //Attack Variables
    private bool attacking = false;
    private float attackRange = 4f;

    //Basic Attack Variables
    private int basicAtkDmg = 10;
    private float attackAnimationLength = 0.4f;

    //Heavy Attack Variables
    private int heavyAtkDmg = 50;

    void Start()
    {
        particles.Stop();
        particles2.Stop();
        animator = GetComponentInParent<Animator>();
        enemyMask = LayerMask.GetMask("Enemy");
    }

    IEnumerator fadeInLight()
    {
        while (attacking)
        {

            if (combatLight.intensity < baseIntensity)
                combatLight.intensity += 1.5f;

            yield return new WaitForSeconds(0.05f);
        }

    }

    IEnumerator dealDamage()
    {
        particles.Play();

        yield return new WaitForSeconds(attackAnimationLength);

        particles2.Play();
        RaycastHit hit;

        Vector3 castPos = new Vector3(transform.position.x, transform.position.y + yOffset, transform.position.z);

        bool hitEnemy = Physics.Raycast(castPos, transform.forward, out hit, attackRange, enemyMask);
        Debug.DrawRay(castPos, transform.forward, Color.blue, attackRange);

        //Trigger the light
        combatLight.intensity = baseIntensity;
        combatLight.range = 3.5f;

        //If the Raycast hits an object on the layer Enemy
        if (hitEnemy)
        {
            //Hit attack
            GameObject enemy = hit.transform.gameObject;

            enemy.GetComponentInParent<Enemy>().loseHealth(basicAtkDmg);
            Debug.Log("Hit");
        }
        else
        {
            //Miss attack
        }

        particles.Stop();

        yield return new WaitForSeconds(0.1f);

        attacking = false;

        yield return new WaitForSeconds(0.3f);

        particles2.Stop();
    }

	void Update()
    {

        //Basic Attack
        if (Input.GetMouseButtonDown(0) && !attacking){

            attacking = true;

            //Play the attack animation instantly
            animator.Play("Attack");

            StartCoroutine("dealDamage");
            StartCoroutine("fadeInLight");
        }



        //Dim the light from the attack animation
        if (combatLight.intensity > 0)
            combatLight.intensity -= 0.5f;
    }


}



