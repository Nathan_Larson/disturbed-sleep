﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLight : MonoBehaviour {

    //Playre Components
    private PlayerMovement playermovement;

    //Light Component
    private Light[] lights;
    public Image chargeBar;

    //Light Variables
    private float baseIntensity;
    public float charge;
    private float rechargeRate = RechargePad.rechargeRate;
    public float powerLossRate = 0.5f;
    public float powerLossFrequency = 1f; //Rate at which the powerLoss/Gain method is called

	// Use this for initialization
	void Start ()
    {
        playermovement = GetComponent<PlayerMovement>();
        lights = GetComponentsInChildren<Light>();
        charge = 100f;
        baseIntensity = lights[0].intensity;

        InvokeRepeating("powerLoss", 0.1f, powerLossFrequency);

    }

    void powerLoss()
    {
        if(playermovement.isSprining())
            charge -= powerLossRate*2;
        else
            charge -= powerLossRate;

        if (charge < 0)
        {
            charge = 0;
            lights[0].intensity = 0;
            lights[1].intensity = 0;
        }
        else
        {
            lights[0].intensity = ((baseIntensity / 100) * charge);
            lights[1].intensity = ((baseIntensity / 100) * charge);
        }

        chargeBar.fillAmount = charge/100;
    }

    void powerGain()
    {
        charge += rechargeRate;

        if (charge > 100)
        {
            charge = 100;
            lights[0].intensity = baseIntensity;
            lights[1].intensity = baseIntensity;
        }
        else
        {
            lights[0].intensity = ((baseIntensity / 100) * charge);
            lights[1].intensity = ((baseIntensity / 100) * charge);
        }

        chargeBar.fillAmount = charge / 100;
    }

    public void setRecharge(string powerStatus, float chargeFrequency)
    {
        CancelInvoke();

        if (chargeFrequency == 0)
            chargeFrequency = powerLossFrequency;

        InvokeRepeating(powerStatus, 0.5f, chargeFrequency);
    }
	
}
