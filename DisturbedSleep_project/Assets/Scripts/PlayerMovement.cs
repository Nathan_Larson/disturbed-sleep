﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    //Camera Variable
    private Camera cam;

    //Components
    private PlayerLight playerLight;
    private CharacterController cControl;
    private Animator animator;

    //Movement Variables
    private Vector3 movement;
    private float moveSpeed;
    private float walkSpeed = 0.025f;
    private float sprintSpeed = 0.04f;
    public float gravity = 9.8f;

	// Use this for initialization
	void Start ()
    {
        playerLight = GetComponent<PlayerLight>();
        cControl = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        cam = Camera.main;
    }

    public bool isSprining()
    {
        return Input.GetKey(KeyCode.LeftShift) && movement.magnitude > 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameState.paused)
            return;

        // read key inputs
        bool wKeyDown = Input.GetKey(KeyCode.W);
        bool aKeyDown = Input.GetKey(KeyCode.A);
        bool sKeyDown = Input.GetKey(KeyCode.S);
        bool dKeyDown = Input.GetKey(KeyCode.D);
        bool shiftKeyDown = Input.GetKey(KeyCode.LeftShift);

        movement = Vector3.zero;

        if (shiftKeyDown && playerLight.charge > 0)
            moveSpeed = sprintSpeed;
        else
            moveSpeed = walkSpeed;

        if (wKeyDown && ! sKeyDown)
        {
            movement += cam.transform.forward;
        }
        if (sKeyDown && !wKeyDown)
        {
            movement += cam.transform.forward * -1;
        }
        if (aKeyDown && !dKeyDown)
        {
            movement += cam.transform.right * -1;
        }
        if (dKeyDown && !aKeyDown)
        {
            movement += cam.transform.right;
        }

        movement.y = 0;
        movement.y -= gravity * Time.deltaTime;

        cControl.Move(movement * moveSpeed);
        animator.SetFloat("speed", cControl.velocity.magnitude);

    }

}
