﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotate : MonoBehaviour {

    private int floorMask;
    private float camRayLength = 100f;

	// Use this for initialization
	void Start ()
    {
        //So ray cast only takes into account the floor quad
        floorMask = LayerMask.GetMask("Floor");
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (GameState.paused)
            return;

        //Facing Direction
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        //If raycast hits the plane
        if (Physics.Raycast(camRay, out hit, camRayLength ,floorMask))
        {
            Vector3 point = camRay.GetPoint(hit.distance) - (camRay.direction * 1f);

            transform.LookAt(point);
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
        }
    }
}
