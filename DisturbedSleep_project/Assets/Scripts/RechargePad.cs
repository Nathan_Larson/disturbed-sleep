﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargePad : MonoBehaviour {

    public static float rechargeRate = 2;
    private float rechargeFrequency = 0.05f; //Rate at which invoke is called


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponentInChildren<PlayerLight>().setRecharge("powerGain", rechargeFrequency);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponentInChildren<PlayerLight>().setRecharge("powerLoss", 0);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
