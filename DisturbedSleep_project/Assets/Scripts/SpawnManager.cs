﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/**
 * SpawnManager is responsible for making the spawners spawn in prefabs for the game.
 */
public class SpawnManager : MonoBehaviour {

    public float dangerRate;
    public float delay;
    public float spawnChance;
    public GameObject player;
    public List<Spawner>spawners;
    public GameObject[] prefabs;

    private float danger;
    private float nextTime;

    // Use this for initialization
    void Start ()
    {
        danger = 0;
        nextTime = Time.time + delay;

        prefabs = Resources.LoadAll<GameObject>("Prefabs/Enemies");
    }




	// Update is called once per frame
	void Update () {
        if (Time.time >= nextTime)
        {
            nextTime += delay;
            foreach (Spawner spawner in spawners)
            {
                int index = (int)(Random.Range(0.0f,danger));
                GameObject spawnObject = prefabs[index]; ;
                if(true)//if (spawner.Clear(spawnObject, player) && spawnChance > Random.Range(0.0f,1.0f))
                {
                    spawner.SpawnNow(spawnObject);
                    danger = Mathf.Min(danger + dangerRate, prefabs.Length - 0.01f);
                }
            }
        }
	}
}
