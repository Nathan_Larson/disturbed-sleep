﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
public class Spawner : MonoBehaviour {
    
    public float spookDistance;
    
    private Vector3 SpawnHeart(GameObject spawnObject) {
        //find the center of the spawnObject's location
        Vector3 spawnHeart = transform.position;
        spawnHeart.y = spawnHeart.y + spawnObject.transform.lossyScale.y/2;
        return spawnHeart;
    }

    public bool Clear(GameObject spawnObject,GameObject player)
    {
        return Physics.OverlapSphere(SpawnHeart(spawnObject), spawnObject.transform.lossyScale.y / 2  - 0.01f).Length == 0
            && Vector3.Distance(transform.position, player.transform.position) > spookDistance;
    }

    public void SpawnNow(GameObject spawnObject)
    {
        Instantiate(spawnObject,transform.position, transform.rotation);
    }
}
